const mongoose= require("mongoose");
const courseModel = require("../models/course.model");

const createCourse = async (req, res) => {
    const {
        title,
        description,
        noStudent,
        reviewsId
    } = req.body;

    if (!mongoose.Types.ObjectId(reviewsId)) {
        res.status(201).json({
            status: "Bad Request",
            message: "courseId is invalid"
        })
    }
    //stars là số nguyên lớn hơn 0, nhỏ hơn 6
    // Number.isInteger(stars) && stars < 0 && stars > 6
    if (!stars == undefined || Number.isInteger(stars) && stars < 0 && stars > 6) {
        res.status(201).json({
            status: "Bad Request",
            message: "stars is invalid"
        })
    }

    //b3: thao tác với csdl
    var newReview = {
        _id: new mongoose.Types.ObjectId(),
        stars,
        note
    }

    var createReview = await reviewModel.create({ newReview });
    res.status(201).json({
        message: "Create Review Succesfully",
        data: createReview
    })


}

const getAllCourse = (req, res) => {
    res.status(200).json({
        message: "Get All Courses"
    })
}

const getIdCourse = (req, res) => {
    var courseId = req.params.courseId;

    res.status(200).json({
        message: "Get Course by Id " + courseId,
    })
}

const updateCourse = (req, res) => {
    var courseId = req.params.courseId;

    res.status(200).json({
        message: "Update Course: " + courseId,
    })
}

const deleteCourse = (req, res) => {
    var courseId = req.params.courseId;

    res.status(200).json({
        message: "Delete Course: " + courseId,
    })
}


module.exports = {
    createCourse,
    getAllCourse,
    getIdCourse,
    updateCourse,
    deleteCourse
}