const mongoose = require("mongoose");
const reviewModel = require("../models/review.models");
const courseModel = require("../models/course.model");


const createReviewOfCourse = async (req, res) => {
    const {
        stars,
        note,
        courseId
    } = req.body;

    // if(!mongoose.Types.ObjectId(courseId)){
    //     res.status(400).json({
    //         status: "Bad Request",
    //         message: "courseId is invalid"
    //     })
    // }
    //stars là số nguyên lớn hơn 0, nhỏ hơn 6
    // Number.isInteger(stars) && stars < 0 && stars > 6
    if (!stars == undefined || Number.isInteger(stars) && stars < 0 && stars > 6) {
        res.status(400).json({
            status: "Bad Request",
            message: "stars is invalid"
        })
    }

    //b3: thao tác với csdl
    var newReview = {
        _id: new mongoose.Types.ObjectId(),
        stars,
        note
    }

    try {
        var createReview = await reviewModel.create({ newReview });

        console.log(createReview);
        res.status(201).json({
            status: "Create review successfully",
            data
        })
    }
    catch (error) {
        res.status(500).json({
            status: "Bad Request",
            message: error,
        })

    }


    // reviewModel.create({ newReview })
    //     .then((data) => {
    //         //Thêm reviewId vào mảng review của courseId
    //         courseModel.findByIdAndUpdate(courseId, {
    //             $push: { reviews: data._id }
    //         })
    //         res.status(201).json({
    //             status: "Create review successfully",
    //             data
    //         })
    //     });

}

const getAllReviewCourse = (req, res) => {
    res.status(200).json({
        message: "Get All Courses"
    })
}

const getReviewById = (req, res) => {
    var courseId = req.params.courseId;

    res.status(200).json({
        message: "Get Course by Id " + courseId,
    })
}

const updateReviewById = (req, res) => {
    var courseId = req.params.courseId;

    res.status(200).json({
        message: "Update Course: " + courseId,
    })
}

const deleteReviewById = (req, res) => {
    var courseId = req.params.courseId;

    res.status(200).json({
        message: "Delete Course: " + courseId,
    })
}


module.exports = {
    createReviewOfCourse,
    getAllReviewCourse,
    getReviewById,
    updateReviewById,
    deleteReviewById
}