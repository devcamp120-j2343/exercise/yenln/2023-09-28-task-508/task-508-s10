//B1 khai báo thư viện mongoose
const mongoose = require("mongoose")

//B2 Khai báo class Schema
const Schema = mongoose.Schema;

//B3 Khởi tạo schema với các thuộc tính của collection
const reviewSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false,
    },
    courses: [
        {
            type: mongoose.Types.ObjectId,
            ref: "course",
        }
    ]

})

//B4: biên dịch schema thành model
module.exports = mongoose.model("review", reviewSchema)