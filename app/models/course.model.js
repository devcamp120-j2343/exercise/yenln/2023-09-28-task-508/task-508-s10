//B1 khai báo thư viện mongoose
const mongoose = require("mongoose")

//B2 Khai báo class Schema
const Schema = mongoose.Schema;

//B3 Khởi tạo schema với các thuộc tính của collection
const courseSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    title: {
        type: String,
        required: true,
        unique: true,
    },
    description: {
        type: String,
        required: false,
    },
    noStudent: {
        type: Number,
        default: 0,
    },
    reviews: [
        {
            type: mongoose.Types.ObjectId,
            ref: "review",
        }
    ]
})

//B4: biên dịch schema thành model
module.exports = mongoose.model("course", courseSchema)