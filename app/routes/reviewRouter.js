const express = require("express");

const reviewMiddleware = require("../middlewares/review.middleware");
const reviewControllers = require("../controllers/review.controller")

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL reiview: ", req.url);

    next();
});

router.get("/", reviewMiddleware.getAllReviewMiddleware, reviewControllers.getAllReviewCourse)

router.post("/", reviewMiddleware.createReviewMiddleware, reviewControllers.createReviewOfCourse);

router.get("/:reviewId", reviewMiddleware.getDetailReviewMiddleware, reviewControllers.getReviewById);

router.put("/:reviewId", reviewMiddleware.updateReviewMiddleware, reviewControllers.updateReviewById);

router.delete("/:reviewId", reviewMiddleware.deleteReviewMiddleware, reviewControllers.deleteReviewById);

module.exports = router;
